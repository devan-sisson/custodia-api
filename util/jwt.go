package util

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func VerifiyAdmin(jwtString string) bool {
	secretKey := []byte("superSecretKey")
	token, err := jwt.Parse(jwtString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return secretKey, nil
	})

	if err != nil {
		fmt.Println("JWT parsing/verification error:", err)
		return false
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		fmt.Println("Invalid JWT claims")
		return false
	}

	isAdmin := claims["role"] == "admin"

	return isAdmin
}

func GenToken() {
	secretKey := []byte("superSecretKey")

	claims := jwt.MapClaims{
		"username": "admin_user",
		"role":     "admin",
		"exp":      time.Now().Add(time.Hour * 24).Unix(),
	}

	// Create a token with the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign the token with the secret key
	tokenString, err := token.SignedString(secretKey)
	if err != nil {
		fmt.Println("JWT signing error:", err)
		return
	}

	// Print the generated JWT
	fmt.Println("Generated JWT for Testing:")
	fmt.Println(tokenString)
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.URL.String() == "/api/drafts" && r.Method == "POST" {
			authHeader := r.Header.Get("Authorization")

			if authHeader == "" {
				http.Error(w, "Missing or empty Authorization header", http.StatusUnauthorized)
			}

			authHeaderParts := strings.Split(authHeader, " ")
			if len(authHeaderParts) != 2 || authHeaderParts[0] != "Bearer" {
				http.Error(w, "Invalid Authorization header format", http.StatusUnauthorized)
			}

			bearerToken := authHeaderParts[1]

			if ok := VerifiyAdmin(bearerToken); !ok {
				http.Error(w, "Missing or empty Authorization header", http.StatusUnauthorized)
			}
		}

		next.ServeHTTP(w, r)
	})
}
