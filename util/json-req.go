package util

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func JsonReq(req *http.Request, input interface{}) {
	defer req.Body.Close()
	reqData, err := io.ReadAll(req.Body)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(reqData, input)
	if err != nil {
		panic(err)
	}
}

func JsonResp(resp http.ResponseWriter, statusCode int, output interface{}) {
	body, err := json.Marshal(output)

	if err != nil {
		panic(err)
	}

	resp.WriteHeader(statusCode)
	resp.Header().Set("content-type", "application/json")
	resp.Header().Set("content-length", fmt.Sprintf("%d", len(body)))
	_, err = resp.Write(body)

	if err != nil {
		panic(err)
	}
}
