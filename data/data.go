package data

type Input struct {
	Name    string `json:"name"`
	Content string `json:"content"`
}

type SearchFilter struct {
	SearchWords string `json:"searchWords"`
}

type CommentInput struct {
	UserId     string     `json:"userId"`
	ParentId   int        `json:"parentId"`
	ParentType ParentType `json:"parentType"`
	Body       string     `json:"body"`
}

type ReactInput struct {
	CommentId     int            `json:"commentId"`
	UserId        string         `json:"userId"`
	EmojiReaction ReactionSymbol `json:"emojiReaction"`
}

type Draft struct {
	ID        uint   `json:"id" gorm:"primaryKey"`
	CreatedAt string `json:"createdAt" gorm:"defaut:unixepoch()"`
	UpdatedAt string `json:"updatedAt" gorm:"defaut:unixepoch()"`
	Name      string `json:"name"`
	Content   string `json:"content"`
}

type Comment struct {
	ID         uint       `json:"id" gorm:"primaryKey"`
	CreatedAt  string     `json:"createdAt"  gorm:"defaut:unixepoch()"`
	UpdatedAt  string     `json:"updatedAt"  gorm:"defaut:unixepoch()"`
	ParentId   int        `json:"parentId"`
	ParentType ParentType `json:"parentType"`
	Body       string     `json:"body"`
	UserId     string     `json:"userId" gorm:"default:admin"`
}

type Reaction struct {
	ID            uint           `json:"id" gorm:"primaryKey"`
	CreatedAt     string         `json:"createdAt"  gorm:"defaut:unixepoch()"`
	UpdatedAt     string         `json:"updatedAt"  gorm:"defaut:unixepoch()"`
	CommentId     int            `json:"commentId"`
	UserId        string         `json:"userId" gorm:"default:userX"`
	EmojiReaction ReactionSymbol `json:"emojiReaction"`
}

type ReactionSymbol string

const (
	ThumbsUp   ReactionSymbol = "👍"
	ThumbsDown ReactionSymbol = "👎"
)

type ParentType string

const (
	DraftParentType   ParentType = "draft"
	CommentParentType ParentType = "comment"
)
