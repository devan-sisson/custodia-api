module custodia-api

go 1.21.3

require (
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/inflection v1.0.0
	github.com/mattn/go-sqlite3 v1.14.0
)

require github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
