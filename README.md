# Custodia Drafts Api: take home assignment

## TODO

- [x] Add an API for Adding a Draft
- [x] Add an API for fetching the Most Recent Drafts of all Documents
- [x] Add APIs for searching Drafts
- [x] Add Database Model for Commenting on a draft
- [x] Add the Ability to Comment on Comments
- [x] Add Ability to React to Comments with Emojis
- [x] Add [Postman Collection](https://app.getpostman.com/join-team?invite_code=01f6672c0ea33873909f085e9479f55a)
- [x] Add Admin Interface for Adding Comments on a Draft
- [ ] Add tests

### notes
* I have the server print out a JWT that you can use in postman to add an authorization header that will be validated by the middleware. 
* If there are issues with the invite link, Josh Should be an admin on the team I created and would be able to add you to reveiw the Postman collection. If there are any issues, feel free to reach out to me to resolve it.
* There were some life events that precluded me from spending the time on this that it deserves, which included adding tests. I will add tests and they will be available for review, if desired.


## custodia-backend-takehome
Back-end take home assignments


For this assignment, we'll ask you to build out an application for hosting drafts of documents.
Each draft of a document may be commented on by admin users.

Please implement your solution using Golang. We understand you might not already be fluent in Golang; this exercise isn't for us to evaluate your Golang skills, but instead your ability to make excellent design and implementation choices that span all languages, and also your ability to learn new things quickly.

#### Additional Information

For the purposes of this project, it's fine to use sqlite.
Additional database setup and configuration is not required.

#### Requirements

We have a few features that we would like to see implemented.


###### Store Multiple Drafts of a Document in the Database

We would like to be able to store multiple draft of a given document.
Documents should be unique by name and the draft just needs to store a bunch of text and a created at timestamp.
It is safe to assume only one user will be creating drafts, so we don't need to track the creator of the drafts.

###### Add Database Model for Commenting on a Draft

We would like reviewers to be able to leave comments on a particular version of a draft.
A comment just needs a reference to the user who created it, some bit of text and a timestamp for when the comment was created.

###### Add the Ability to Comment on Comments

We would like to be able to comment on comments, so that users can discuss ideas.

###### Add Ability to React to Comments with Emojis

We would like to be able to react to comments using emojis.

###### Add an API for Adding a Draft

Eventually, we may want to build a frontend around this system, so we would like to have an API for submitting a draft.

We would like to the API to work with json uploads.

Here is an example curl call that we would like to make:

```
curl -X POST http://localhost:8080/api/drafts
     -H "Content-Type: application/json"
     --data '{"name": "Cookie Recipe", "content": "Go to Wholefoods"}' ```
```

###### Add Admin Interface for Adding Comments on a Draft

Admin users should be able to comment on a particular draft.
*I'm not really sure what this means.*

###### Add an API for fetching the Most Recent Drafts of all Documents

We want to be able to fetch and display the most recent drafts of all documents.
Please add an API that allows you to fetch the most recent version of all documents.

Here is an example curl call that we would like to use to fetch the most recent draft:

curl http://localhost:8080/api/drafts

```
[
    {
        name: "Cookie Recipe",
        text: "1 cup of flour, 1 cup of chocolate chips, check google for the rest",
    },
    {
        name: "Resume",
        text: "I like computers",
    },
]
```


###### Add APIs for searching Drafts

Add an API for searching drafts for particular strings of text.

###### Add tests

Curl commands are great for testing out APIs, but who has time for that? Computers, do, that's who.

The next ask is that you provide some tests for the application. We would like to see tests for API interaction, but feel
free to add any additional testing that you think the code could use.


###### Add Postman collection

We would like to be able to test the functionality of the API, so let's add a [Postman collection](https://www.postman.com/collection/)
that covers all major API functionality.
