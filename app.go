package main

import (
	"custodia-api/data"
	"custodia-api/util"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var db *gorm.DB

func main() {
	var err error
	db, err = gorm.Open("sqlite3", "test.db")
	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()

	db.AutoMigrate(&data.Draft{}, &data.Comment{}, &data.Reaction{})

	//Generates the jwt to be used in testing
	util.GenToken()

	r := mux.NewRouter()

	r.Use(util.AuthMiddleware)
	r.HandleFunc("/api/drafts", HandleGetDrafts).Methods("GET")
	r.HandleFunc("/api/drafts/search", HandleSearchDrafts).Methods("POST")
	r.HandleFunc("/api/comment", HandleComment).Methods("POST")
	r.HandleFunc("/api/react", HandleReact).Methods("POST")

	r.HandleFunc("/api/drafts", HandleCreateDraft).Methods("POST")

	http.Handle("/", r)

	fmt.Println("Server is running on :8080")
	http.ListenAndServe(":8080", nil)
}

func HandleCreateDraft(w http.ResponseWriter, r *http.Request) {
	var input data.Input
	util.JsonReq(r, &input)

	result := db.Create(&data.Draft{
		CreatedAt: time.Now().String(),
		UpdatedAt: time.Now().String(),
		Name:      input.Name,
		Content:   input.Content,
	})

	if result.Error != nil {
		panic(result.Error)
	}

	util.JsonResp(w, 200, result)
}

func HandleGetDrafts(w http.ResponseWriter, r *http.Request) {
	var drafts []data.Draft
	db.Model(data.Draft{}).
		Select("id, name, content, MAX(created_at) as created_at").
		Group("name").
		Find(&drafts)

	util.JsonResp(w, 200, drafts)
}

func HandleSearchDrafts(w http.ResponseWriter, r *http.Request) {
	var searchFilter data.SearchFilter
	util.JsonReq(r, &searchFilter)

	var drafts []data.Draft
	db.Model(data.Draft{}).
		Select("*").
		Where("content LIKE ?", "%"+searchFilter.SearchWords+"%").
		Find(&drafts)

	util.JsonResp(w, 200, drafts)
}

func HandleComment(w http.ResponseWriter, r *http.Request) {
	var input data.CommentInput
	util.JsonReq(r, &input)

	result := db.Create(&data.Comment{
		CreatedAt:  time.Now().String(),
		UpdatedAt:  time.Now().String(),
		ParentId:   input.ParentId,
		ParentType: input.ParentType,
		UserId:     input.UserId,
		Body:       input.Body,
	})

	if result.Error != nil {
		panic(result.Error)
	}

	util.JsonResp(w, 200, result)
}

func HandleReact(w http.ResponseWriter, r *http.Request) {
	var input data.ReactInput
	util.JsonReq(r, &input)

	result := db.Create(&data.Reaction{
		CreatedAt:     time.Now().String(),
		UpdatedAt:     time.Now().String(),
		UserId:        input.UserId,
		CommentId:     input.CommentId,
		EmojiReaction: input.EmojiReaction,
	})

	if result.Error != nil {
		panic(result.Error)
	}

	util.JsonResp(w, 200, result)
}
